"use strict";

let inputPrice = document.querySelector('.input-price');
let container = document.querySelector('.container');
let error = document.querySelector('.input-error');

let inputText = document.createElement('span');
inputText.classList.add('input-text');

inputPrice.addEventListener('blur', function() {
  inputText.remove();
  error.style.display = 'none';
  
  let inputPriceValue = +inputPrice.value;
  if (inputPriceValue > 0) {
    inputText.innerHTML = `Current price:${inputPriceValue}<span class="text-close"></span>`;
    container.insertBefore(inputText, container.firstChild);
    inputPrice.style.color = 'green';
    let textClose = document.querySelector('.text-close');
    textClose.addEventListener('click', function() {
      inputText.remove();
      inputPrice.value = '';
    })
  } else {
    error.style.display = 'block';
    inputPrice.style.border = '1px solid red';
  }
})