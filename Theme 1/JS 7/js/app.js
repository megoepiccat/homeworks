"use strict";

const array = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];

let func = (array) => {
	let arrayMap = array.map(function(item) {
  	return `<li>${item}</li>`
  }).join('');
  let markup = `<ul>${arrayMap}</ul>`;
	document.body.innerHTML = markup;
}

func(array);