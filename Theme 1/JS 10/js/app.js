"use strict";

let showPass = document.querySelectorAll('.js-icon-password');
let submitBtn = document.querySelector('.js-btn');
let inputs = document.querySelectorAll('.js-label input');
let error = document.querySelector('.js-error'); 

showPass.forEach(function(icon) {
	icon.addEventListener('click', function() {
		let input = this.closest('.js-label').querySelector('input');
		let inputTypePassword = input.type;
		if (inputTypePassword === 'password') {
			input.type = 'text';
			this.classList.add('fa-eye-slash');
			this.classList.remove('fa-eye');
		} else {
			input.type = 'password';
			this.classList.remove('fa-eye-slash');
			this.classList.add('fa-eye');
		};
	});
});

submitBtn.addEventListener('click', function(e) {
	e.preventDefault();
	if (inputs[0].value === inputs[1].value) {
		error.style.display = 'none';
		alert('You are welcome!');
	} else {
		error.style.display = 'block';
	};
});