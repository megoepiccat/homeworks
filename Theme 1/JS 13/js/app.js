"use strict";

let changeThemeButton = document.querySelector('.theme');
let body = document.querySelector('body');
let currentTheme = localStorage.getItem('theme');

if (currentTheme == 2) {
  body.classList.add('theme-two');
};

changeThemeButton.onclick = function () {
  if (body.classList.contains('theme-two')) {
    body.classList.remove('theme-two');
    localStorage.setItem('theme', 1);
  } else {
    body.classList.add('theme-two');
    localStorage.setItem('theme', 2);
  }
};