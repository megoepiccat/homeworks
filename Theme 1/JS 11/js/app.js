"use strict";

let buttons = document.querySelectorAll('.btn');

document.addEventListener('keypress', (e) => {
	const keyName = e.key.toLowerCase();

	buttons.forEach(function (button) {
		let currentButton = button.innerHTML.toLowerCase();

		button.classList.remove('btn-active');

		if (currentButton === keyName) {
			button.classList.add('btn-active');
		};
	});
});