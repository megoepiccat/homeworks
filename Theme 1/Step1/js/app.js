'use strict'

class Carousel {
  constructor() {
    this.slidesCount =  $('.js-slider-item').length;
    this.nextSlide = null;
    this.init()
  }

  init() {
    $('.js-slide-prev').on('click', () => {
        this.changeSlides('prev');
    });

    $('.js-slide-next').on('click', () => {
      this.changeSlides('next');
    });

    $('.js-slider-item').on('click', (e) => {
      let currentIndex = $(e.currentTarget).attr('data-index');

      $('.js-slider-item').removeClass('is-active-slide')
      $(e.currentTarget).addClass('is-active-slide');
      this.changeInfo(currentIndex);
    });
  }

  changeSlides(direction) {
    let currentSlide = $('.js-slider-item.is-active-slide');
    let currentSlideIndex = +currentSlide.attr('data-index');

    currentSlide.removeClass('is-active-slide');

    if (direction == 'prev') {
      if (currentSlideIndex == 0) {
        this.nextSlide = $(`.js-slider-item[data-index="${this.slidesCount - 1}"]`);
        this.nextSlide.addClass('is-active-slide');
      } else {
        this.nextSlide = $(`.js-slider-item[data-index="${currentSlideIndex - 1}"]`);
        this.nextSlide.addClass('is-active-slide');
      }
    } else {
      if (currentSlideIndex < (this.slidesCount - 1)) {
        this.nextSlide = $(`.js-slider-item[data-index="${currentSlideIndex + 1}"]`);
        this.nextSlide.addClass('is-active-slide');
      } else {
        this.nextSlide = $(`.js-slider-item[data-index="0"]`);
        this.nextSlide.addClass('is-active-slide');
      }
    }

    let newSlideIndex = this.nextSlide.attr('data-index');

    this.changeInfo(newSlideIndex);
  };

  changeInfo(index) {
    let currentInfo = $(`.js-info-item[data-index="${index}"]`);

    $('.js-info-item').removeClass('is-active-info');
    currentInfo.addClass('is-active-info');
  }
}

class Amazing {
  constructor() {
    this.showMoreBtn = $('.js-load-more');
    this.amazingItems = $('.js-amazing-item');
    this.amazingBtn = $('.js-amazing-btn');
    this.activeCategory = '';
    this.setActiveTab();
    this.clickEvents();
  }

  setActiveTab() {
    const _this = this;

    this.amazingBtn.each(function () {
      if ($(this).hasClass('is-active')) {
        _this.activeCategory = $(this).attr('data-category');
        _this.showActiveTab(_this.activeCategory);
      }
    });
  };

  showActiveTab(activeCategory) {
    let activeTabs = $(`.js-amazing-item[data-category="${activeCategory}"]`);

    this.amazingItems.addClass('is-hide');
    this.showMoreBtn.addClass('is-hide');

    if (activeTabs.length > 11 || this.activeCategory === 'All') {
      this.showMoreBtn.removeClass('is-hide');
    }

    if (this.amazingItems.length > 11) {
      if (activeCategory === 'All') {
        this.amazingItems.each((index, item) => {
          if (index <= 11) {
            $(item).removeClass('is-hide');
          }
        })
      } else {
        activeTabs.removeClass('is-hide');
      }
    } else {
      if (activeCategory === 'All') {
        this.amazingItems.removeClass('is-hide');
      } else {
        activeTabs.removeClass('is-hide');
      }
    };
  }

  clickEvents() {
    const _this = this;

    this.showMoreBtn.on('click', function() {
      _this.activeTabs = $(`.js-amazing-item[data-category="${_this.activeCategory}"]`);
      
      if (_this.activeCategory === 'All') {
        _this.amazingItems.each(function (index, item) { 
          if (index > 11) {
            console.log(index);
            $(item).toggleClass('is-hide');
          };
        });
      } else {
        if (_this.activeTabs.length > 11) {
          _this.amazingItems.each(function (index, item) { 
            if (index > 11) {
              $(item).toggleClass('is-hide');
            };
          });
        } else {
          _this.activeTabs.removeClass('is-hide');
        }
      }
    })
    
    
    this.amazingBtn.on('click', function() {
      _this.activeCategory = $(this).attr('data-category');
      _this.amazingBtn.removeClass('is-active');
      $(this).addClass('is-active');
    
      _this.showActiveTab(_this.activeCategory);
    })
  }
}

class Tabs {
  constructor() {
    this.buttons = $('.js-tab-btn');
    this.tabs = $('.js-tab');
    this.init()
  }

  init() {

    $('body').on('click', '.js-tab-btn', (e) => { 
      let activeBtn = e.currentTarget;
      let activeTabIndex = $(activeBtn).attr('data-index');

      if ($(activeBtn).hasClass('is-tab-active')) return

      $(this.buttons).removeClass('is-tab-active');
      $(this.tabs).addClass('is-hidden');
      $(activeBtn).addClass('is-tab-active');
      
      this.tabs.each((i, item) => {
        if ($(item).attr('data-index') === activeTabIndex) {
          $(item).removeClass('is-hidden');
        }
      })

    });
  }
    
}

new Tabs();
new Amazing();
new Carousel();