do {
	let num1 = prompt("Введите первое число");
	let num2 = prompt("Введите второе число");
	let operation = prompt("Выберите операцию (+ или - или * или /)");

	condition =
		!isNaN(+num1) &&
		num1 !== null &&
		num1 !== "" &&
		!isNaN(+num2) &&
		num2 !== null &&
		num2 !== "";

	operations =
		operation === "+" ||
		operation === "-" ||
		operation === "*" ||
		operation === "/";

	if (condition && operations) {
		switch (operation) {
			case "+":
				console.log(
					`Над числами ${num1} и ${num2} была произведена операция ${operation}. Результат: ${+num1 + +num2}.`
				);
				break;
			case "-":
				console.log(
					`Над числами ${num1} и ${num2} была произведена операция ${operation}. Результат: ${num1 - num2}.`
				);
				break;
			case "/":
				console.log(
					`Над числами ${num1} и ${num2} была произведена операция ${operation}. Результат: ${num1 / num2}.`
				);
				break;
			case "*":
				console.log(
					`Над числами ${num1} и ${num2} была произведена операция ${operation}. Результат: ${num1 * num2}.`
				);
				break;
		}
	}
} while (!condition && !operations);
