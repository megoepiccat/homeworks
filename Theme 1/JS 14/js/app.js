"use strict";

let links = $('.js-links');

links.each(function () {  
  $(this).click(function(e) {
    e.preventDefault();
    let currentSection = $(this).attr('href');
    console.log($(currentSection).offset().top);
    $('html, body').animate({
        scrollTop: $(currentSection).offset().top
    }, 1000);
  });
});