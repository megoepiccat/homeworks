"use strict";

let tabsButtons = document.querySelectorAll('.tabs__link');
let tabsPane = document.querySelectorAll('.tabs__pane');

window.addEventListener('click', function(e) {
  let currentButton = e.target;
  if (currentButton.classList.contains('tabs__link')) {
		e.preventDefault;
		let currentButtonData = currentButton.getAttribute('href');
		let currentPane = document.querySelector(currentButtonData);
		for (let i = 0; i < tabsButtons.length; i++) {
				tabsButtons[i].classList.remove('tabs__link_active');
		}
		for (let i = 0; i < tabsPane.length; i++) {
				tabsPane[i].classList.remove('tabs__pane_show');
		}
		currentButton.classList.add('tabs__link_active');
		currentPane.classList.add('tabs__pane_show');
	}
});