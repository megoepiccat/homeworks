"use strict";

let slides = document.querySelectorAll('#slides .slide');
let currentSlide = 0;
let slideInterval = setInterval(nextSlide,1000);
let playing = true;
let pauseButton = document.getElementById('pause');
let continueButton = document.getElementById('continue');

function nextSlide(){
	slides[currentSlide].className = 'slide';
	currentSlide = (currentSlide+1)%slides.length;
	slides[currentSlide].className = 'slide showing';
}

function pauseSlideshow(){
	playing = false;
	clearInterval(slideInterval);
}

function playSlideshow(){
	playing = true;
	slideInterval = setInterval(nextSlide,1000);
}

pauseButton.onclick = function(){
	if(playing){ pauseSlideshow(); }
};

continueButton.onclick = function(){
	if(!playing){ playSlideshow(); }
};